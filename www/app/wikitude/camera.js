var camera = {
  toggleCamera: function toggleCamera(){
    if(AR.hardware.camera.enabled==true){
      AR.hardware.camera.enabled = false
    } else {
      AR.hardware.camera.enabled = true;
    }
  },
  stopCamera: function stopCamera(){
    AR.hardware.camera.enabled = false
  },
  startCamera: function startCamera(){
    AR.hardware.camera.enabled = true
  },
};