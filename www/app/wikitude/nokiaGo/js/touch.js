var Touch = {
  loaded: false,
  oldEventScale : 0,
  lastTouch: {
    x: 0,
    y: 0
  },
  swipeAllowed: true,
  interactionContainer: 'renderCanvas',

  init: function initFn() {
    Touch.steadyElement = document.getElementById('steady-container');

    if(document.location.pathname=="/android_asset/www/app/wikitude/nokiaGo/"){ //android
      this.inWorld = true;
    } else if(document.location.pathname.includes("nokiaGo")){ // ios
      this.inWorld = true;
    }
    Touch.showSteadyElement();
    var imgPrefix = '';
    if(this.inWorld){
      imgPrefix = "../";
    } else {
      console.log("addInteractionEventListener");
      this.renderCanvas = document.getElementById(Touch.interactionContainer);
      this.addInteractionEventListener();
    }
    hudTemplate.createTemplate(imgPrefix);
  },

  showSteadyElement:function(){
    if(this.inWorld){
      Touch.steadyElement.classList.remove("fadeOut");
      Touch.steadyElement.classList.add("fadeIn");
      setTimeout(function(){
        Touch.hideSteadyElement();
      },5000)
    }
  },
  hideSteadyElement:function(){
    Touch.steadyElement.classList.remove("fadeIn");
    Touch.steadyElement.classList.add("fadeOut");
  },


  /*
   Event handler for touch and gesture events. The handler are used to calculate and set new rotate and scaling values for the 3D model.
   */
  handleTouchStart : function handleTouchStartFn(event) {
    if (event.touches.length > 1)
      return;

    Touch.swipeAllowed = true;

    /* Once a new touch cycle starts, keep a save it's originating location */
    Touch.lastTouch.x = event.touches[0].clientX;
    Touch.lastTouch.y = event.touches[0].clientY;

    /*event.preventDefault();*/
  },

  handleTouchMove : function handleTouchMoveFn(event) {
    /*			console.log(event.touches.length);*/
    if (event.touches.length > 1)
      return;

    if (Touch.swipeAllowed) {

      /* Define some local variables to keep track of the new touch location and the movement between the last event and the current one */
      var touch = {
        x: event.touches[0].clientX,
        y: event.touches[0].clientY
      };
      var movement = {
        x: 0,
        y: 0
      };

      /* Calculate the touch movement between this event and the last one */
      movement.x = (Touch.lastTouch.x - touch.x) * -1;
      movement.y = (Touch.lastTouch.y - touch.y) * -1;

      babylonMode.rotateModel(movement.x,movement.y);

      /* Keep track of the current touch location. We need them in the next move cycle */
      Touch.lastTouch.x = touch.x;
      Touch.lastTouch.y = touch.y;
    }

    /*event.preventDefault();*/
  },

  changeFocusPoint : function(focusPoint){
    x = Touch.tempFocusPoint.x - focusPoint.x;
    y = focusPoint.y - Touch.tempFocusPoint.y;
    babylonMode.moveModel(x, y);
  },

  loadingStep: function loadingStepFn() {
    console.log('loadingStep');
    if (!Touch.loaded && Touch.tracker.isLoaded() && Touch.model.isLoaded()) {
      Touch.loaded = true;
      console.log('World loaded');
      if (Touch.trackableVisible && !Touch.appearingAnimation.isRunning()) {
        Touch.appearingAnimation.start();
      }
    }
  },

  hideLeftHud: function(){
    document.getElementById("left_hud").classList.add("hidden");
  },
  showLeftHud: function(){
    document.getElementById("left_hud").classList.remove("hidden");
  },

  toggleRightHud: function() {
    document.getElementById("burger_btn").classList.toggle("slide_out");
    document.getElementById("burger_btn_icon").classList.toggle("slide_out");
    document.getElementById("prod_info").classList.toggle("slide_out");
    document.getElementById("prod_info2").classList.toggle("slide_out");
    document.getElementById("right_hud_bottom_orange").classList.toggle("slide_out");
    document.getElementById("func_wrapper").classList.toggle("slide_out");
    document.getElementById("bg").classList.toggle("slide_out");
    document.getElementById("right_hud_e_components").classList.toggle("slide_out");
  },

  toggleDemos: function() {
    var eles = document.getElementsByClassName("demos_btn"), i=eles.length;
    while(i--) {
      eles[i].classList.toggle("hidden");
    }
    this.unselectOther("demos_btn");


  },

  toggleComponents: function(highlight) {
    if(highlight && !this.inWorld){
      babylonMode.hightLightMeshes();
    }
    if(document.getElementById("prod_info2").classList.contains("slide_out")){
      document.getElementById("right_hud_e_components").classList.toggle("slide_out");
      document.getElementById("right_hud_e_components").classList.toggle("deactive");
      document.getElementById("burger_btn").classList.toggle("hidden");
    } else {
      document.getElementById("right_hud_e_components").classList.toggle("deactive");
      document.getElementById("prod_info").classList.toggle("deactive");
    }
    document.getElementById("right_hud_e").classList.toggle("deactive");
    document.getElementById("back_btn").classList.toggle("deactive");
    document.getElementById("burger_btn").classList.toggle("deactive");
    setTimeout(function(){
      document.getElementById("burger_btn").classList.toggle("deactive");
    },500);
  },

  toggle3d: function() {
    console.log(this.inWorld);
    if(this.inWorld){
      document.location = 'architectsdk://action?close';
    } else {
      console.log('loadWorld');
      app.loadWorld();
    }
  },

  unselectOther: function(className) {
    var eles = document.getElementsByClassName("func_btn_wrapper"), i = eles.length;
    while(i--) {
      if(!eles[i].classList.contains(className)) {
        if(eles[i].classList.contains("px0")) {
          eles[i].classList.add("hidden");
        } else {
          eles[i].classList.remove("hidden");
        }
      }
    }
  },
  unselectOtherComponents: function(className) {
    var eles = document.getElementsByClassName("func_component_btn_wrapper"), i = eles.length;
    while(i--) {
      if(!eles[i].classList.contains(className)) {
        if(eles[i].classList.contains("px0")) {
          eles[i].classList.add("hidden");
        } else {
          eles[i].classList.remove("hidden");
        }
      }
    }
  },
  togglehead: function(enable){
    //todo set left hud content
    this.showLeftHud();
    var eles = document.getElementsByClassName("head_view_btn"), i=eles.length;
    while(i--) {
      eles[i].classList.toggle("hidden");
    }
    this.unselectOtherComponents("head_view_btn");
    if(this.inWorld){
      if(enable){
        World.toggleMesh('Head')
      } else {
        World.toggleMesh('Complete')
      }
    } else {
      if(enable){
        babylonMode.selectMesh('Head')
      } else {
        babylonMode.selectMesh('Default')
      }
    }
    document.getElementById('component_name').innerHTML = babylonMode.descriptions.head.title;
    document.getElementById('component_desc').innerHTML = babylonMode.descriptions.head.description;

    if(!enable){
      Touch.hideLeftHud();
    }
  },
  torso: function(enable){
    //todo set left hud content
    this.showLeftHud();
    var eles = document.getElementsByClassName("torso_view_btn"), i=eles.length;
    while(i--) {
      eles[i].classList.toggle("hidden");
    }
    this.unselectOtherComponents("torso_view_btn");

    if(this.inWorld){
      if(enable){
        World.toggleMesh('Torso')
      } else {
        World.toggleMesh('Complete')
      }
    } else {
      if(enable){
        babylonMode.selectMesh('Torso')
      } else {
        babylonMode.selectMesh('Default')
      }
    }

    document.getElementById('component_name').innerHTML = babylonMode.descriptions.torso.title;
    document.getElementById('component_desc').innerHTML = babylonMode.descriptions.torso.description;
    if(!enable){
      Touch.hideLeftHud();
    }
  },
  complete: function(enable) {
    //todo set left hud content
    this.showLeftHud();
    var eles = document.getElementsByClassName("complete_view_btn"), i=eles.length;
    while(i--) {
      eles[i].classList.toggle("hidden");
    }
    this.unselectOtherComponents("complete_view_btn");
    if(this.inWorld){
      World.toggleMesh('Complete')
    } else {
      babylonMode.selectMesh('Default')
    }

    document.getElementById('component_name').innerHTML = babylonMode.descriptions.complete.title;
    document.getElementById('component_desc').innerHTML = babylonMode.descriptions.complete.description;
    if(!enable){
      Touch.hideLeftHud();
    }
  },
  toggleEyes: function(enable){
    //todo set left hud content
    this.showLeftHud();
    var eles = document.getElementsByClassName("eyes_view_btn"), i=eles.length;
    while(i--) {
      eles[i].classList.toggle("hidden");
    }
    this.unselectOtherComponents("eyes_view_btn");

    if(this.inWorld){
      if(enable){
        World.toggleMesh('Eyes')
      } else {
        World.toggleMesh('Head')
      }
    } else {
      if(enable){
        babylonMode.selectMesh('Eyes')
      } else {
        babylonMode.selectMesh('Head')
      }
    }

    document.getElementById('component_name').innerHTML = babylonMode.descriptions.eyes.title;
    document.getElementById('component_desc').innerHTML = babylonMode.descriptions.eyes.description;
    if(!enable){
      Touch.hideLeftHud();
    }
  },
  toggleMouth: function(enable){
    //todo set left hud content
    this.showLeftHud();
    var eles = document.getElementsByClassName("mouth_view_btn"), i=eles.length;
    while(i--) {
      eles[i].classList.toggle("hidden");
    }
    this.unselectOtherComponents("mouth_view_btn");

    if(this.inWorld){
      if(enable){
        World.toggleMesh('Mouth')
      } else {
        World.toggleMesh('Head')
      }
    } else {
      if(enable){
        babylonMode.selectMesh('Mouth')
      } else {
        babylonMode.selectMesh('Head')
      }
    }

    document.getElementById('component_name').innerHTML = babylonMode.descriptions.mouth.title;
    document.getElementById('component_desc').innerHTML = babylonMode.descriptions.mouth.description;
    if(!enable){
      Touch.hideLeftHud();
    }
  },
  toggleHelmet: function(enable){
    //todo set left hud content
    this.showLeftHud();
    var eles = document.getElementsByClassName("helmet_view_btn"), i=eles.length;
    while(i--) {
      eles[i].classList.toggle("hidden");
    }
    this.unselectOtherComponents("helmet_view_btn");

    if(this.inWorld){
      if(enable){
        World.toggleMesh('Helmet')
      } else {
        World.toggleMesh('Head')
      }
    } else {
      if(enable){
        babylonMode.selectMesh('Helmet')
      } else {
        babylonMode.selectMesh('Head')
      }
    }

    document.getElementById('component_name').innerHTML = babylonMode.descriptions.helmet.title;
    document.getElementById('component_desc').innerHTML = babylonMode.descriptions.helmet.description;
    if(!enable){
      Touch.hideLeftHud();
    }
  },

  getVector: function(event){
    return BABYLON.Vector3.Unproject(
      new BABYLON.Vector3(event.center.x, event.center.y, 0),
      window.innerWidth, window.innerHeight,
      BABYLON.Matrix.Identity(),
      babylonMode.scene.getViewMatrix(),
      babylonMode.scene.getProjectionMatrix()
    );
  },
  /*
   Touch and gesture listener are added to allow rotation and scale changes in the snapped to screen state.
   */
  addInteractionEventListener: function addInteractionEventListenerFn() {
    Touch.renderCanvas.addEventListener('touchstart', Touch.handleTouchStart, false);
    Touch.renderCanvas.addEventListener('touchmove', Touch.handleTouchMove, false);

    var myElement = document.getElementById('renderCanvas');
    var mc = new Hammer.Manager(myElement);

    var pinch = new Hammer.Pinch();
    var pan2 = new Hammer.Pan({event: 'pan2',pointers: 2, threshold: 2});
    pan2.recognizeWith(pinch);


    var adjustScale = babylonMode.scale;
    var currentScale = babylonMode.scale;
    mc.add([pinch, pan2]);
    mc.on("pinchstart", function (event) {
      this.swipeAllowed = false;
      Touch.renderCanvas.removeEventListener('touchstart', this.handleTouchStart, false);
      Touch.renderCanvas.removeEventListener('touchmove', this.handleTouchMove, false);
      Touch.lastTouchCenter = Touch.getVector(event);
    });
    mc.on("pinch", function (event) {
      if(this.oldEventScale != event.scale){
        currentScale = adjustScale * event.scale;
        babylonMode.scaleModel(currentScale);
      }
      var touchCenter = Touch.getVector(event);
      var factor = 15;
      var x = Touch.lastTouchCenter.x - touchCenter.x;
      var y = Touch.lastTouchCenter.y - touchCenter.y;
      var mesh = babylonMode.scene.getMeshByName("Bumble");
      mesh.position.x = mesh.position.x - x * factor;
      mesh.position.y = mesh.position.y - y * factor;
      console.log("x:" +babylonMode.scene.meshes[0].position.x);
      console.log("y:" +babylonMode.scene.meshes[0].position.y);
      Touch.lastTouchCenter = touchCenter;
    });

    mc.on("pinchend", function (event) {
      this.swipeAllowed = true;
      this.oldEventScale = currentScale;
      adjustScale = currentScale;

      setTimeout(function(){
        mc.add(pan2);
        Touch.renderCanvas.addEventListener('touchstart', this.handleTouchStart, false);
        Touch.renderCanvas.addEventListener('touchmove', this.handleTouchMove, false);
      },50);

    });

    /* IOS EVENTS */
    /*Touch.renderCanvas.addEventListener('gesturestart', Touch.handleGestureStart, false);
     Touch.renderCanvas.addEventListener('gesturechange', Touch.handleGestureChange, false);
     Touch.renderCanvas.addEventListener('gestureend', Touch.handleGestureEnd, false);*/
  },
  removeInteractionEventListener: function removeInteractionEventListenerFn() {
    Touch.renderCanvas.removeEventListener('touchstart', Touch.handleTouchStart, false);
    Touch.renderCanvas.removeEventListener('touchmove', Touch.handleTouchMove, false);

    /* IOS EVENTS */
    Touch.renderCanvas.removeEventListener('gesturestart', Touch.handleGestureStart, false);
    Touch.renderCanvas.removeEventListener('gesturechange', Touch.handleGestureChange, false);
    Touch.renderCanvas.removeEventListener('gestureend', Touch.handleGestureEnd, false);
  }
};
