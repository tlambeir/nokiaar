var World = {
  steadyElement:null,
  loaded: false,
  inFieldOfVision:false,
  rotating: false,
  trackableVisible: false,
  snapped: false,
  oldEventScale : 0,
  lastTouch: {
    x: 0,
    y: 0
  },
  lastScale: 1,
  currentScale: 0,
  swipeAllowed: true,
  interactionContainer: 'renderCanvas',

  init: function initFn() {
    AR.logger.activateDebugMode();
    this.createOverlays();
    World.steadyElement = document.getElementById('steady-container');
    Touch.showSteadyElement();
  },

  appear: function appearFn() {
    AR.logger.debug('appear');
    World.trackableVisible = true;
    if (World.loaded && !World.snapped) {
      // Resets the properties to the initial values.
      World.resetModel();
      World.appearingAnimation.start();
    }
  },

  disappear: function disappearFn() {
    World.trackableVisible = false;
    World.model.scale = {
      x: 0,
      y: 0,
      z: 0
    };
    World.head.scale = {
      x: 0,
      y: 0,
      z: 0
    };
    World.Torso.scale = {
      x: 0,
      y: 0,
      z: 0
    };
  },

  createOverlays: function createOverlaysFn() {
    console.log('createOverlaysFn');
    /*
     First an AR.ClientTracker needs to be created in order to start the recognition engine. It is initialized with a URL specific to the target collection. Optional parameters are passed as object in the last argument. In this case a callback function for the onLoaded trigger is set. Once the tracker is fully loaded the function loadingStep() is called.

     Important: If you replace the tracker file with your own, make sure to change the target name accordingly.
     Use a specific target name to respond only to a certain target or use a wildcard to respond to any or a certain group of targets.
     */
    this.tracker = new AR.ClientTracker("assets/tracker.wtc", {
      onLoaded: this.loadingStep
    });

    /*this.backGround = new AR.HtmlDrawable({uri:"../markers/background.html"}, 5, {});*/
    /*
     3D content within Wikitude can only be loaded from Wikitude 3D Format files (.wt3). This is a compressed binary format for describing 3D content which is optimized for fast loading and handling of 3D content on a mobile device. You still can use 3D models from your favorite 3D modeling tools (Autodesk® Maya® or Blender) but you'll need to convert them into the wt3 file format. The Wikitude 3D Encoder desktop application (Windows and Mac) encodes your 3D source file. You can download it from our website. The Encoder can handle Autodesk® FBX® files (.fbx) and the open standard Collada (.dae) file formats for encoding to .wt3.

     Create an AR.Model and pass the URL to the actual .wt3 file of the model. Additional options allow for scaling, rotating and positioning the model in the scene.

     A function is attached to the onLoaded trigger to receive a notification once the 3D model is fully loaded. Depending on the size of the model and where it is stored (locally or remotely) it might take some time to completely load and it is recommended to inform the user about the loading time.
     */
    this.model = new AR.Model("assets/Bumble_Textured.wt3", {

      onLoaded: this.loadingStep,
      /*
       The drawables are made clickable by setting their onClick triggers. Click triggers can be set in the options of the drawable when the drawable is created. Thus, when the 3D model onClick: this.toggleAnimateModel is set in the options it is then passed to the AR.Model constructor. Similar the button's onClick: this.toggleAnimateModel trigger is set in the options passed to the AR.ImageDrawable constructor. toggleAnimateModel() is therefore called when the 3D model or the button is clicked.

       Inside the toggleAnimateModel() function, it is checked if the animation is running and decided if it should be started, resumed or paused.
       */
      scale: {
        x: 0,
        y: 0,
        z: 0
      },
      translate: {
        x: 0.0,
        y: -0.5,
        z: 0
      },
      rotate: {
        roll: 0,
        tilt:0
      }
    });

    this.activeModel = this.model;

    this.head = new AR.Model("assets/Bumble_Head.wt3", {
      onLoaded: this.loadingStep,
      scale: {
        x: 0,
        y: 0,
        z: 0
      },
      translate: {
        x: 0.0,
        y: -0.5,
        z: 0
      },
      rotate: {
        roll: 0,
        tilt:0
      }
    });

    this.Torso = new AR.Model("assets/Bumble_Torso.wt3", {
      onLoaded: this.loadingStep,
      scale: {
        x: 0,
        y: 0,
        z: 0
      },
      translate: {
        x: 0.0,
        y: -0.4,
        z: 0
      },
      rotate: {
        roll: 0,
        tilt:0
      }
    });
    /*
     As a next step, an appearing animation is created. For more information have a closer look at the function implementation.
     */
    this.appearingAnimation = this.createAppearingAnimation(this.model, 0.10);

    this.onEnterFieldOfVision = function onEnterFieldOfVision() {
      World.inFieldOfVision = true;
      AR.logger.debug('onEnterFieldOfVision');
      var focusFrame = document.getElementById("focusFrame");
      focusFrame.classList.add("greenify");
      focusFrame.classList.add("pauze-animation");
      setTimeout(function(){
        focusFrame.classList.add("hidden");
        if(!World.snapped){
          World.appear();
          World.showModeElement();
        }
      }.bind(this), 1500);
    };

    this.onExitFieldOfVision = function onExitFieldOfVision() {
      Touch.showSteadyElement();
      World.inFieldOfVision = false;
      console.log('onExitFieldOfVision');
      var focusFrame = document.getElementById("focusFrame");
      focusFrame.classList.remove("pauze-animation");
      focusFrame.classList.remove("greenify");
      if(!World.snapped){
        focusFrame.classList.remove("hidden");
      }
      World.disappear();
    };

    /*
     To receive a notification once the image target is inside the field of vision the onEnterFieldOfVision trigger of the AR.Trackable2DObject is used. In the example the function appear() is attached. Within the appear function the previously created AR.AnimationGroup is started by calling its start() function which plays the animation once.

     To add the AR.ImageDrawable to the image target together with the 3D model both drawables are supplied to the AR.Trackable2DObject.
     */
    this.trackable = new AR.Trackable2DObject(this.tracker, "*", {
      drawables: {
        cam: [this.activeModel]
      },
      onEnterFieldOfVision: this.onEnterFieldOfVision,
      onExitFieldOfVision: this.onExitFieldOfVision
    });
  },

  loadingStep: function loadingStepFn() {
    console.log('loadingStep');
    if (!World.loaded && World.tracker.isLoaded() && World.model.isLoaded()) {
      World.loaded = true;
      console.log('World loaded');
      Touch.init();
      if (World.trackableVisible && !World.appearingAnimation.isRunning()) {
        World.appearingAnimation.start();
      }
    }
  },

  createAppearingAnimation: function createAppearingAnimationFn(model, scale) {
    /*
     The animation scales up the 3D model once the target is inside the field of vision. Creating an animation on a single property of an object is done using an AR.PropertyAnimation. Since the model needs to be scaled up on all three axis, three animations are needed. These animations are grouped together utilizing an AR.AnimationGroup that allows them to play them in parallel.

     Each AR.PropertyAnimation targets one of the three axis and scales the model from 0 to the value passed in the scale variable. An easing curve is used to create a more dynamic effect of the animation.
     */
    var sx = new AR.PropertyAnimation(model, "scale.x", 0, scale, 1500, {
      type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_BACK
    });
    var sy = new AR.PropertyAnimation(model, "scale.y", 0, scale, 1500, {
      type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_BACK
    });
    var sz = new AR.PropertyAnimation(model, "scale.z", 0, scale, 1500, {
      type: AR.CONST.EASING_CURVE_TYPE.EASE_OUT_BACK
    });

    return new AR.AnimationGroup(AR.CONST.ANIMATION_GROUP_TYPE.PARALLEL, [sx, sy, sz],{onFinish : function(){
      World.model.scale = {
        x: scale,
        y: scale,
        z: scale
      };
    }});
  },

  resetModel: function resetModelFn() {
    World.rotating = false;
  },

  toggleMesh: function(name){
    World.trackable.drawables.removeCamDrawable(0);
    switch (name){
      case "Head":
        console.log("switching to Head");
        World.head.scale = World.activeModel.scale;
        World.head.rotate = World.activeModel.rotate;
        World.trackable.drawables.addCamDrawable(World.head);
        World.activeModel = World.head;
        break;
      case "Complete":
        console.log("switching to Complete");
        World.model.scale = World.activeModel.scale;
        World.model.rotate = World.activeModel.rotate;
        World.trackable.drawables.addCamDrawable(World.model);
        World.activeModel = World.model;
        break;
      case "Torso":
        console.log("switching to Torso");
        World.Torso.scale = World.activeModel.scale;
        World.Torso.rotate = World.activeModel.rotate;
        World.trackable.drawables.addCamDrawable(World.Torso);
        World.activeModel = World.Torso;
        break;
    }
  },
  /*
   This function is used to either snap the trackable onto the screen or to detach it. World.trackable.snapToScreen.enabled is therefore used. Depending on the snap state a new layout for the position and size of certain drawables is set. To allow rotation and scale changes only in the snapped state, event handler are added or removed based on the new snap state.
   */
  toggleSnapping: function toggleSnappingFn() {
    AR.logger.debug("toogleSnapping start");
    World.snapped = !World.snapped;

    var focusFrame = document.getElementById("focusFrame");

    var renderCanvas = document.getElementById("renderCanvas");
    if (World.snapped) {
      World.hideSteadyElement();
      World.trackable.drawables.removeCamDrawable(World.activeModel);
      focusFrame.classList.add("hidden");
      AR.hardware.camera.enabled = false;
    } else {
      focusFrame.classList.remove("hidden");
      World.trackable.drawables.addCamDrawable(World.activeModel);
      AR.hardware.camera.enabled = true;
    }
  },
};

World.init();
