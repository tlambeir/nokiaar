var babylonMode = {
  descriptions: {
    complete: {
      title: "Complete",
      description: "The Bumblebee is a fictional character from the Transformers franchise. In most incarnations, Bumblebee is a small, yellow (with black stripes) Autobot with most of his alternative vehicle modes inspired by several generations of the Chevrolet Camaro/Pontiac Firebird muscle cars."
    },
    torso: {
      title: "Torso",
      description: "The torso or trunk is an anatomical term for the central part of the many animal bodies (including that of the human) from which extend the neck and limbs.[1] The torso includes the thorax and the abdomen."
    },
    head: {
      title: "Head",
      description: "A head is the part of an organism which usually comprises the eyes, ears, nose and mouth, each of which aid in various sensory functions such as sight, hearing, smell, and taste. Some very simple animals may not have a head, but many bilaterally symmetric forms do."
    },
    eyes: {
      title: "Eyes",
      description: "Eyes are organs of the visual system. They provide organisms vision, the ability to process visual detail, as well as enabling several photo response functions that are independent of vision. Eyes detect light and convert it into electro-chemical impulses in neurons. "
    },
    mouth: {
      title: "Mouth",
      description: "In biological anatomy, commonly referred to as the mouth, under formal names such as the oral cavity, buccal cavity, or in Latin cavum oris,[1] is the opening through which many animals take in food and issue vocal sounds. It is also the cavity lying at the upper end of the alimentary canal, bounded on the outside by the lips and inside by the pharynx and containing in higher vertebrates the tongue and teeth."
    },
    helmet: {
      title: "Helmet",
      description: "A helmet is a form of protective gear worn to protect the head from injuries. More specifically, a helmet aids the skull in protecting the human brain. Ceremonial or symbolic helmets (e.g. UK policeman's helmet) without protective function are sometimes used. "
    }
  },
  // Here begins a function that we will 'call' just after it's built
  scale: 0.5,
  lastPosition: {
    x: 0,
    y: 0
  },
  createScene: function () {
    BABYLON.SceneLoader.Load("", "assets/babylon/3Mesh_3SubMesh_Parent.babylon", this.engine, function (newScene) {

      // Wait for textures and shaders to be ready
      newScene.executeWhenReady(function () {

        console.dir(newScene);

        newScene.clearColor = new BABYLON.Color4(0, 0, 0, 0.0000000000000001);

        var camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(0, 5, -20), newScene);

        camera.setTarget(BABYLON.Vector3.Zero());

        newScene.activeCamera = camera;

        // Attach camera to canvas inputs
        /*newScene.activeCamera.attachControl(this.canvas);*/
        var bumbleMesh = newScene.getMeshByName("Bumble");
        bumbleMesh.scaling = new BABYLON.Vector3(babylonMode.scale, babylonMode.scale, babylonMode.scale);


        babylonMode.hl = new BABYLON.HighlightLayer("hl1", newScene);

        for (var i = 0; i < newScene.meshes.length; i++) {
          console.log('loading mesh: ' + newScene.meshes[i].name);
          newScene.meshes[i].isPickable = true;
          newScene.meshes[i].actionManager = new BABYLON.ActionManager(newScene);
          newScene.meshes[i].oldMaterial = newScene.meshes[i].material;
          newScene.meshes[i].actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, function (ev) {
            console.log('OnPickTrigger');
            var mesh = ev.meshUnderPointer;
            switch (mesh.name) {
              case "Head":
                Touch.togglehead(mesh.material.name != 'Transparent' ? true : false);
                break;
              case "Torso":
                Touch.torso(mesh.material.name != 'Transparent' ? true : false);
                break;
              default:
                Touch.complete(mesh.material.name != 'Transparent' ? true : false);
                break;
            }
          }));
        }

        babylonMode.activeMesh = newScene.meshes[0];

        babylonMode.scene = newScene;

        // Once the scene is loaded, just register a render loop to render it
        babylonMode.engine.runRenderLoop(function () {
          babylonMode.scene.render();
        });

      });
    });
  },

  hightLightMeshes : function () {
    var alpha = 0;
    this.scene.registerBeforeRender(function() {
      alpha += 0.2;

      babylonMode.hl.blurHorizontalSize = 0.3 + Math.cos(alpha) * 0.6 + 0.6;
      babylonMode.hl.blurVerticalSize = 0.3 + Math.sin(alpha / 3) * 0.6 + 0.6;
    });
    for (var i = 0; i < this.scene.meshes.length; i++) {
      var mesh = this.scene.meshes[i];
      if(mesh.name != 'Bumble'){
        this.flickerMesh(mesh, i);
      }
    }
  },

  flickerMesh : function(mesh, i){
    setTimeout(function(){
      babylonMode.hl.addMesh(mesh, BABYLON.Color3.White());
      setTimeout(function(){
        babylonMode.hl.removeMesh(mesh);
      },750);
    },(i * 750));
  },

  selectMesh: function (name) {
    console.log("selecting mesh:" + name);
    for (var i = 0; i < this.scene.meshes.length; i++) {
      var mesh = this.scene.meshes[i];
      var changeMaterial = false;
      if (mesh.name == name || name == 'Default') {
        changeMaterial = true;
      } else if (mesh.parent && mesh.parent.name == name) {
        changeMaterial = true;
      }
      if (changeMaterial) {
        mesh.material = mesh.oldMaterial;
      } else if (mesh.name != 'Bumble') {
        var materialTransparent = new BABYLON.StandardMaterial("Transparent", babylonMode.scene);
        materialTransparent.alpha = 0.5;
        mesh.material = materialTransparent;
      }
    }
    switch (name) {
      default:
        this.moveModelToPosition(0, 0, new BABYLON.Vector3(0.5, 0.5, 0.5))
        break;
      case "Torso":
        this.moveModelToPosition(0, -5, new BABYLON.Vector3(1.5, 1.5, 1.5));
        break;
      case "Head":
        this.moveModelToPosition(0, -18, new BABYLON.Vector3(3.14, 3.14, 3.14));
        break;
      case "Mouth":
        this.moveModelToPosition(0, -21, new BABYLON.Vector3(4.3, 4.3, 4.3));
        break;
      case "Helmet":
        this.moveModelToPosition(0, -18, new BABYLON.Vector3(3.14, 3.14, 3.14));
        break;
      case "Eyes":
        this.moveModelToPosition(0, -24, new BABYLON.Vector3(4.3, 4.3, 4.3));
        break;
    }
  },
  moveModelToPosition: function (x, y, scale) {
    var mesh = this.scene.getMeshByName("Bumble");

    var animationX = new BABYLON.Animation("myAnimationX", "position.x", 50, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    // An array with all animation keys
    animationX.setKeys([
      {
        frame: 0,
        value: mesh.position.x
      }, {
        frame: 100,
        value: x
      }
    ]);

    var animationY = new BABYLON.Animation("myAnimationY", "position.y", 50, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    // An array with all animation keys
    animationY.setKeys([
      {
        frame: 0,
        value: mesh.position.y
      }, {
        frame: 100,
        value: y
      }
    ]);

    var animationScale = new BABYLON.Animation("myAnimationScale", "scaling", 50, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    // An array with all animation keys
    animationScale.setKeys([
      {
        frame: 0,
        value: mesh.scaling
      }, {
        frame: 100,
        value: scale
      }
    ]);

    var animationRotation = new BABYLON.Animation("myAnimationRotation", "rotation", 50, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    // An array with all animation keys
    animationRotation.setKeys([
      {
        frame: 0,
        value: mesh.rotation
      }, {
        frame: 100,
        value: new BABYLON.Vector3(0, 0, 0)
      }
    ]);

    mesh.animations = [animationX, animationY, animationScale, animationRotation];

    this.scene.beginAnimation(mesh, 0, 100, false);
  },

  reset: function () {
    for (var i = 0; i < this.scene.meshes.length; i++) {
      this.scene.meshes[i].rotation.y = 0;
      this.scene.meshes[i].rotation.x = 0;
      this.scene.meshes[i].scaling = new BABYLON.Vector3(this.scale, this.scale, this.scale);
    }
  },
  rotateModel: function (movementX, movementY) {
    var mesh = this.scene.getMeshByName("Bumble");
    var calculatedHeading = mesh.rotation.y - (movementX * 0.02);
    var calculatedTilt = mesh.rotation.x - (movementY * 0.02);
    mesh.rotation.y = calculatedHeading;
    if ((calculatedTilt > -0.4) && (calculatedTilt < 0.6)) {
      mesh.rotation.x = calculatedTilt;
    }
  },
  scaleModel: function (scale) {
    var mesh = this.scene.getMeshByName("Bumble");
    mesh.scaling = new BABYLON.Vector3(scale, scale, scale);
    this.scale = scale;
    console.log('scale: ' + scale);
  },
  moveModel: function (x, y) {
    console.log("move: " + x / 100);
    for (var i = 0; i < this.scene.meshes.length; i++) {
      this.scene.meshes[i].position.x = x / 100 + this.scene.meshes[i].position.x;
      console.log("position: " + this.scene.meshes[i].position.x);
      // this.scene.meshes[i].position.y = this.scene.meshes[i].position.y - y;
    }
  },
  init: function () {
    this.canvas = document.querySelector("#renderCanvas");
    // Load the BABYLON 3D engine
    this.engine = new BABYLON.Engine(this.canvas, true, { stencil: true });
    this.createScene();
    // Watch for browser/canvas resize events
    window.addEventListener("resize", function () {
      babylonMode.engine.resize();
    });
  }
};
/*babylonMode.init();*/
