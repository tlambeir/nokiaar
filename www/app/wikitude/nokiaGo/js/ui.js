angular.module('ui', ["ngRoute", "ngSanitize", "ngTouch"]).config(function ($routeProvider) {
  $routeProvider.when('/', {controller: 'uiController', templateUrl: 'ui.html'});
})
  .controller('uiController', function ($scope) {
    $scope.toggleSnapping = function(){
      World.toggleSnapping();
    };
  });