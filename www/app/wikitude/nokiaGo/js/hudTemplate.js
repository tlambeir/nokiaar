var hudTemplate = {
  imgPrefix : '',
  createTemplate: function(imgPrefix){
    hudTemplate.imgPrefix = imgPrefix;
    console.log(hudTemplate.imgPrefix);
    var template = '\
      <div class="left_hud hidden" id="left_hud">\
          <div class="left_hud_n">\
              <div class="prod_name">\
                <span id="component_name"></span>\
              </div>\
              <div class="left_hud_x_wrapper">\
                  <img src="'+imgPrefix+'images/hud/Cancel.png" alt="" class="left_hud_x" onclick="Touch.hideLeftHud();">\
              </div>\
          </div>\
          <div class="left_hud_s">\
              <div class="prod_des" id="component_desc"></div>\
          </div>\
      </div>\
      <div class="right_hud">\
          <div class="right_hud_w">\
              <div class="right_hud_top" id="prod_info">\
                  <div class="right_hud_left">\
                      <div class="func_btn" id="burger_btn" onclick="Touch.toggleRightHud()">\
                          <img src="'+imgPrefix+'images/hud/Hamburger.png" class="func_btn_icon" id="burger_btn_icon">\
                      </div>\
                  </div>\
              </div>\
              <div class="right_hud_bottom_orange"></div>\
          </div>\
          <div class="right_hud_e_components deactive" id="right_hud_e_components">\
            <div class="back_btn_wrapper " id="back_btn_wrapper">\
                <div class="back_btn deactive" id="back_btn" onclick="Touch.complete();Touch.hideLeftHud();Touch.toggleComponents();">\
                    <img src="'+imgPrefix+'images/hud/Back_White.png" class="func_btn_icon" id="back_btn_icon">\
                </div>\
            </div>\
            <div class="components_hud" id="components_hud">\
            </div>\
            <div class="back_btn_wrapper" style="opacity:0;"></div>\
          </div>\
          <div class="right_hud_e" id="right_hud_e">\
              <div class="right_hud_top" id="prod_info2">\
                  <div class="right_hud_right top" id="prod_info3">\
                      <div class="prod_name">Nokia Product Name</div>\
                      <div class="prod_desc">\
                          "Lorem cat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."\
                      </div>\
                  </div>\
              </div>\
              <div class="right_hud_bottom_orange" id="right_hud_bottom_orange">\
                  <div id="bg"></div>\
                  <div class="right_hud_right bottom" id="func_wrapper">' + (Touch.inWorld ? '\
                      <div class="func_btn_wrapper 3dview_btn test" onclick="Touch.toggle3d()">\
                          <div class="func_btn">\
                              <img src="'+imgPrefix+'images/hud/3D_Orange.png" class="func_btn_icon" id="viewmode_btn">\
                          </div>\
                          <div class="func_btn_text">3D View</div>\
                      </div>'  : '\
                      <div class="func_btn_wrapper px0 3dview_btn" onclick="Touch.toggle3d()">\
                          <div class="func_btn">\
                              <img src="'+imgPrefix+'images/hud/AR_White.png" class="func_btn_icon" id="viewmode_btn2">\
                          </div>\
                          <div class="func_btn_text">AR View</div>\
                      </div>') + '\
                      <div class="func_btn_wrapper components_btn" onclick="Touch.toggleComponents(true)">\
                          <div class="func_btn">\
                              <img src="'+imgPrefix+'images/hud/Components_Orange.png" class="func_btn_icon components_btn_icon" id="components_btn">\
                          </div>\
                          <div class="func_btn_text">Components</div>\
                      </div>\
                  </div>\
              </div>\
          </div>\
      </div>\
    ';
    document.getElementById("hud_wrapper").innerHTML = template;
    hudTemplate.createComponentHud('firstLevel');
  },
  createComponentHud: function(parent) {
    var imgPrefix = hudTemplate.imgPrefix;
    console.log(imgPrefix);
    var firstLevel = '\
          <div class="prod_name_wrapper">\
              <div class="prod_name">Nokia Product Name</div>\
          </div>\
          <div class="components_btn_wrapper">\
              <div class="func_btn_wrapper func_component_btn_wrapper torso_view_btn" onclick="Touch.torso(true);">\
                  <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_Orange.png" class="func_btn_icon torso_view_btn_icon" id="torso_view_btn_icon">\
                  </div>\
                  <div class="func_btn_text">Torso</div>\
              </div>\
              <div class="func_btn_wrapper func_component_btn_wrapper px0 hidden torso_view_btn" onclick="Touch.torso();">\
                  <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_White.png" class="func_btn_icon torso_view_btn_icon" id="torso_view_btn_icon2">\
                  </div>\
                  <div class="func_btn_text">Torso</div>\
              </div>\
              <div class="func_btn_wrapper func_component_btn_wrapper head_view_btn" onclick="Touch.togglehead(true);hudTemplate.createComponentHud(\'subHead\');">\
                  <div class="func_btn">\
                    <img src="'+imgPrefix+'images/hud/Components_Parent_Orange.png" class="func_btn_icon" id="">\
                  </div>\
                  <div class="func_btn_text">Head</div>\
              </div>\
              <div class="func_btn_wrapper func_component_btn_wrapper px0 hidden head_view_btn" onclick="Touch.togglehead();">\
                    <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_Parent_White.png" class="func_btn_icon" id="">\
                    </div>\
                    <div class="func_btn_text">Head</div>\
              </div>\
              <div class="func_btn_wrapper func_component_btn_wrapper complete_view_btn" onclick="Touch.complete(true);">\
                  <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_Orange.png" class="func_btn_icon complete_view_btn_icon" id="complete_view_btn_icon">\
                  </div>\
                  <div class="func_btn_text">Complete</div>\
              </div>\
              <div class="func_btn_wrapper func_component_btn_wrapper px0 hidden complete_view_btn" onclick="Touch.complete();">\
                  <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_White.png" class="func_btn_icon complete_view_btn_icon" id="complete_view_btn_icon2">\
                  </div>\
                  <div class="func_btn_text">Complete</div>\
              </div>\
          </div>\
    ';
    var subHead = '\
          <div class="prod_name_wrapper">\
              <div class="prod_name">Head</div>\
          </div>\
          <div class="components_btn_wrapper">\
              <div class="func_btn_wrapper func_component_btn_wrapper eyes_view_btn" onclick="Touch.toggleEyes(true);">\
                  <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_Orange.png" class="func_btn_icon eyes_view_btn_icon" id="eyes_view_btn_icon">\
                  </div>\
                  <div class="func_btn_text">Eyes</div>\
              </div>\
              <div class="func_btn_wrapper func_component_btn_wrapper px0 hidden eyes_view_btn" onclick="Touch.toggleEyes();">\
                  <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_White.png" class="func_btn_icon eyes_view_btn_icon" id="eyes_view_btn_icon2">\
                  </div>\
                  <div class="func_btn_text">Eyes</div>\
              </div>\
              <div class="func_btn_wrapper func_component_btn_wrapper mouth_view_btn" onclick="Touch.toggleMouth(true);">\
                  <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_Orange.png" class="func_btn_icon" id="">\
                  </div>\
                  <div class="func_btn_text">Mouth</div>\
              </div>\
              <div class="func_btn_wrapper func_component_btn_wrapper px0 hidden mouth_view_btn" onclick="Touch.toggleMouth();">\
                  <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_White.png" class="func_btn_icon mouth_view_btn_icon" id="mouth_view_btn_icon">\
                  </div>\
                  <div class="func_btn_text">Mouth</div>\
              </div>\
              <div class="func_btn_wrapper func_component_btn_wrapper helmet_view_btn" onclick="Touch.toggleHelmet(true);">\
                  <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_Orange.png" class="func_btn_icon helmet_view_btn_icon" id="helmet_view_btn_icon">\
                  </div>\
                  <div class="func_btn_text">Helmet</div>\
              </div>\
              <div class="func_btn_wrapper func_component_btn_wrapper px0 hidden helmet_view_btn" onclick="Touch.toggleHelmet();">\
                  <div class="func_btn">\
                      <img src="'+imgPrefix+'images/hud/Components_White.png" class="func_btn_icon helmet_view_btn_icon" id="helmet_view_btn_icon2">\
                  </div>\
                  <div class="func_btn_text">Helmet</div>\
              </div>\
          </div>\
    ';
    if(parent == 'firstLevel'){
      document.getElementById("components_hud").innerHTML = firstLevel;
      document.getElementById("back_btn").onclick = function(){
        Touch.complete();
        Touch.hideLeftHud();
        Touch.toggleComponents()
      };
    } else if(parent == 'subHead'){
      document.getElementById("components_hud").innerHTML = subHead;
      document.getElementById("back_btn").onclick = function(){
        hudTemplate.createComponentHud('firstLevel');
        Touch.togglehead(true);
      };
    }
  }
};