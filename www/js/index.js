/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {

    requiredFeatures: ["2d_tracking"],
    arExperienceUrl: "www/app/wikitude/nokiaGo/index.html",
    //arExperienceUrl: "www/app/wikitude/CloudRecognition/index.html",
    isDeviceSupported: false,
    startupConfiguration:
    {
        "camera_position": "back",
    },
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        app.wikitudePlugin = cordova.require("com.wikitude.phonegap.WikitudePlugin.WikitudePlugin");
        app.wikitudePlugin.isDeviceSupported(app.onDeviceSupported, app.onDeviceNotSupported, app.requiredFeatures);
    },
    onDeviceSupported: function(){
        app.loadWorld();
        Touch.init();
    },

    loadWorld:function(){
        app.wikitudePlugin.setOnUrlInvokeCallback(app.onURLInvoked);

        // set a callback for android that is called once the back button was clicked.
        if ( cordova.platformId == "android" ) {
            app.wikitudePlugin.setBackButtonCallback(app.onBackButton);
        }

        app.wikitudePlugin.loadARchitectWorld(
            app.onARExperienceLoadedSuccessfully,
            app.onARExperienceLoadError,
            app.arExperienceUrl,
            app.requiredFeatures,
            app.startupConfiguration
        );
    },
    onURLInvoked: function(url) {
        if ( 'close' == url.substring(22) ) {
            app.wikitudePlugin.close();
            babylonMode.init();
            Touch.init();
        }
    },
    onDeviceNotSupported: function(){
        alert("Device doesn't support Wikitude plugin");
    },
    onARExperienceLoadedSuccessfully: function(loadedURL){
        console.log("Successfully loaded ARchitectWorld: [loadedURL]");
        // send the AR World to the back so we can see Angular UI
        /*helper.sendToBack();*/
    },
    onARExperienceLoadError: function(errorMessage){
        alert("Failed to load ARchitectWorld!\n" + errorMessage);
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {

    }
};

app.initialize();